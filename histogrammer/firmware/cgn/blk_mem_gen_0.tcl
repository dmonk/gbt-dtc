generate_target synthesis [get_files blk_mem_gen_0.xci]
set_property used_in_synthesis false [get_files -of_object [get_files blk_mem_gen_0.xci] -filter {FILE_TYPE == XDC}]
set_property used_in_implementation false [get_files -of_object [get_files blk_mem_gen_0.xci] -filter {FILE_TYPE == XDC}]
