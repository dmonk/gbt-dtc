----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/29/2019 03:44:40 PM
-- Design Name: 
-- Module Name: TestBench - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.constants.all;


entity TestBench is
end TestBench;

architecture Behavioral of TestBench is
    signal clk : std_logic := '0';
    signal counter : integer := 0; -- Counter to more easily calculate latency
    signal stream_in : std_logic_vector(input_width - 1 downto 0) := (others => '0');
    signal reset : std_logic := '0';
    signal data_out : std_logic_vector(bin_width - 1 downto 0) := (others => '0');
begin

    clk <= not clk after 12.5 ns;

    -- Process for increasing counter by one each clock cycle
    process(clk)
    begin
        if rising_edge(clk) then
            counter <= counter + 1;
        end if;
    end process;
    
    pReset: process(clk)
    begin
        if counter = 16#10000# or counter = 16#8000# then
            reset <= '1';
        end if;
        if reset = '1' then
            reset <= '0';
        end if;
    end process pReset;


    SourceGeneratorInstance : entity work.SourceGenerator
    port map (
        clk => clk,
        data_out => stream_in
    );

    AlgoInstance : entity work.top
    PORT MAP(
        clk => clk,
        data_in => stream_in,
        reset => reset,
        
        data_out => data_out
    );

end Behavioral;
