----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/29/2019 10:41:13 AM
-- Design Name: 
-- Module Name: constants - 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

package constants is
    constant eta_width      : integer := 7;
    constant phi_width      : integer := 7;
    constant input_width    : integer := eta_width + phi_width;
    
    constant bin_width      : integer := 32;
    
    constant one_second     : integer := 6*40*1000000;
    constant lumi_section_seconds  : integer := 93;
end package constants;
