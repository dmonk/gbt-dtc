----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/29/2019 03:22:53 PM
-- Design Name: 
-- Module Name: SourceGenerator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_textio.all;
use std.textio.all;

use work.constants.all;


entity SourceGenerator is
    port (
        clk : in std_logic;
        data_out : out std_logic_vector(input_width - 1 downto 0) := (others => '0')
    );
end SourceGenerator;

architecture Behavioral of SourceGenerator is
    signal vline : std_logic_vector(15 downto 0);
begin
    -- Process that reads file and outputs a line each clock cycle as a logic
    -- vector. NOTE: Option to also write to file is commented out but kept for
    -- future reference.
    process is
        variable line_v : line;
        file read_file : text;
        -- file write_file : text;
        variable slv_v : std_logic_vector(15 downto 0);
    begin
        file_open(read_file, "input.txt", read_mode);
        -- file_open(write_file, "target.txt", write_mode);
        while not endfile(read_file) loop
            wait until clk = '1' and clk'event;
            readline(read_file, line_v);
            hread(line_v, slv_v);
            data_out <= slv_v(input_width - 1 downto 0);
--            data_out(3 downto 0) <= slv_v(3 downto 0);
            vline <= slv_v;
            -- report "slv_v: " & to_hstring(slv_v);
            -- hwrite(line_v, slv_v);
            -- writeline(write_file, line_v);
        end loop;
        file_close(read_file);
        -- file_close(write_file);
        wait;
    end process;

end Behavioral;
