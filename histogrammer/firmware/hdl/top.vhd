----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/26/2019 05:04:33 PM
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.constants.all;


entity top is
    port (
        clk: in std_logic;
        data_in : in std_logic_vector(input_width - 1 downto 0);
        reset : in std_logic;
        data_out : out std_logic_vector(bin_width - 1 downto 0) := (others => '0')
    );
end top;

architecture Behavioral of top is
    -- Internal reset signals
    signal reset_done : std_logic := '1';
    signal reset_counter : unsigned(input_width - 1 downto 0);
    
    -- RAM Port A signals
    signal read_address : std_logic_vector(input_width - 1 downto 0):= (others => '0');
    signal read_value : std_logic_vector(bin_width -1 downto 0) := (others => '0');
    signal read_cache : std_logic_vector(bin_width -1 downto 0) := (others => '0');
    
    -- RAM Port B signals
    signal write_address : std_logic_vector(input_width - 1 downto 0):= (others => '0');
    signal write_address_cache : std_logic_vector(input_width - 1 downto 0) := (others => '0');
    signal write_value : std_logic_vector(bin_width -1 downto 0) := (others => '0');
    
    -- Internal collision mitigation signals
    signal collision : std_logic := '0';
    signal offset : integer := 1;
    
    -- Dummy signals for unused ports
    signal dummy_signal_a : std_logic_vector(bin_width -1 downto 0);
    signal dummy_signal_b : std_logic_vector(bin_width -1 downto 0);
    
    
    -- IP Core generated using wizard. If input_width or bin_width change in the future, the core must be reconfigured.
    component blk_mem_gen_0
    port (
        clka : in std_logic;
        wea : in std_logic_vector(0 DOWNTO 0);
        addra : in std_logic_vector(13 DOWNTO 0); -- = input_width - 1
        dina : in std_logic_vector(31 DOWNTO 0); -- = bin_width - 1
        douta : out std_logic_vector(31 DOWNTO 0);
        clkb : in std_logic;
        web : in std_logic_vector(0 DOWNTO 0);
        addrb : in std_logic_vector(13 DOWNTO 0);
        dinb : in std_logic_vector(31 DOWNTO 0);
        doutb : out std_logic_vector(31 DOWNTO 0)
    );
    end component;
begin
   
    pMain : process(clk)
    begin
        if rising_edge(clk) then
            if (reset = '1' or reset_done = '0') then
                -- Clear counter and flag before starting reset. Start condition should be: reset = '1' and reset_done = '1'. reset signal should be strobed to initiate reset.
                if reset_done = '1' then
                    reset_counter <= (others => '0');
                    reset_done <= '0';
                end if;
                -- Condition for ending reset.
                if reset_counter = (2**input_width - 1) then
                    reset_done <= '1';
                end if;
                -- Main block for reading and resetting RAM.
                if reset_done = '0' then
                    read_address <= std_logic_vector(reset_counter);
                    data_out <= read_value; -- Stream RAM data out
                    
                    write_address <= std_logic_vector(reset_counter);
                    write_value <= (others => '0'); -- Write zeros to the same address
                    
                    reset_counter <= reset_counter + 1;
                end if;
            -- If not resetting, the input data stream must be placed into the histogram
            else
                read_address <= data_in;
                
                -- As the RAM I/O takes two clock cycles, it is possible that the input address repeats during this period. 
                -- Condition 1 is when the write address = read address.
                -- Condition 2 is when there are two consectutive identical addresses
                
                if read_address = write_address then -- Condition 1
                    collision <= '1';
                    read_cache <= write_value; -- Value to be written is cached and used on the next clock cycle instead of the value read from the RAM.
                else
                    collision <= '0';
                end if;
                
                write_address_cache <= read_address; -- Address is cached for a clock cycle to cover the RAM latency.
                write_address <= write_address_cache;
                
                if read_address = write_address_cache then -- Condition 2
                    -- In this case, the value read from the RAM, will be one less than it should be as the value has not yet been updated, therefore
                    -- the offset should be increased by one to account for this.
                    offset <= 2;
                else
                    offset <= 1;
                end if;
                
                if collision = '1' then
                    write_value <= std_logic_vector(unsigned(read_cache) + offset);
                else
                    write_value <= std_logic_vector(unsigned(read_value) + offset);
                end if;
                
                data_out <= (others => '0');
            end if;
        end if;
    end process pMain;
    
    HistogramRAM : blk_mem_gen_0
    port map (
        clka => clk,
        wea => "0",
        addra => read_address,
        dina => dummy_signal_a,
        douta => read_value,
        clkb => clk,
        web => "1",
        addrb => write_address,
        dinb => write_value,
        doutb => dummy_signal_b
    );
    
end Behavioral;